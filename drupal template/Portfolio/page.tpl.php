<?php
$path = drupal_get_path('theme', 'Portfolio');
$url = request_uri();
$url = substr($url, 9);
$home = $GLOBALS['base_path'];

?>

<body>
<div id="header" class="row">
    <div class="large-3 columns">
        <a href="<?php echo $home;?>"><img src="<?php echo $home;?><?php echo $path ?>/img/Logo.png"></a>
    </div>
    <div class="large-9 columns">
        <ul class="right button-group">
    <?php print render($page['header']); ?>

        </ul>
    </div>
    <hr>
</div>

<div id="wrapper">

    <div id="content" class="row">
        <?php if (drupal_is_front_page()): ?>
            <div class="row" id="homemenu">
                <div class="large-4 columns">
                    <img src="<?php echo $home;?><?php echo $path ?>/img/blog.jpg">
                    <h4>Take a look at my blog</h4>

                    <p>Here you can read my latest blogs. These our subject who inspire me. Most of the
                    subjects are about electronics and computers. Getting interested already ?
                        <br><a href="<?php echo $home;?>blog">Come take a look</a></p>
                </div>
                <div class="large-4 columns">
                    <img src="<?php echo $home;?><?php echo $path ?>/img/project.jpg">
                    <h4>See my projects</h4>

                    <p>This is a collection of the work I already did in my life. It goes from design for paper to webdesign.
                        If you have any questions about my work, don't hesitate to ask below.
                        <br> <a href="<?php echo $home;?>projects">Watch my latest work</a></p>
                </div>
                <div class="large-4 columns">
                    <img src="<?php echo $home;?><?php echo $path ?>/img/about.jpg">
                    <h4>Learn who I am</h4>

                    <p>I'm a young webdesigner from Belgium. Want to learn a little bit more about me?
                        <br><a href="<?php echo $home;?>about">Let's see who I am</a> </p>
                </div>
            </div>
            <div class="row">
                <div class="large-12 columns" id="poll">

                </div>
            </div>
            <div class="row">
                <div class="large-12 columns">
                    <div class="panel">
                        <h4>Get in touch!</h4>

                        <div class="row">
                            <div class="large-9 columns">
                                <p>I'd love to hear from you</p>
                            </div>
                            <div class="large-3 columns">
                                <a href="./contact" class="radius button right">Contact Me</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         <?php else: ?>
            <h3><?php echo $title?></h3>
            <?php if ($url == "blog"): ?>
            <a href="./blog/feed" class="right">Click here to follow my blog <img id="rss" src=<?php echo $path ?>/img/rss.png /></a>
            <?php endif;?>
            <?php print render($page['content']); ?>
        <?php endif; ?>


    </div>
</div>

<div id="about">
    <?php if ($url == "about") : ?>
    <div class="row">
        <div class="large-4 columns"><img src=<?php echo $path ?>/img/me.jpg> </div>
        <div class="large-8 columns">
            <p>Ik ben Tim Van Meirvenne, een 22-jarige studente Grafische en Digitale Media aan de Arteveldehogeschool te Gent. Momenteel ga ik voor de afstudeerrichting Multimediaproductie.</p>
            <h2>Skills</h2>
            <ul>
                <li>HTML5</li>
                <li>CSS3</li>
                <li>Javascript</li>
                <li>jQuery</li>
                <li>PHP</li>
                <li>WordPress</li>
                <li>Drupal</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <h2>Situering</h2>
        </div>
        <div class="large-12 columns">
            <div id="map"></div>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <h2>Twitter</h2>
        </div>
        <div class="large-12 columns">
            <a class="twitter-timeline"  href="https://twitter.com/ArteveldehsGent" data-widget-id="598212889158283264">Tweets door @ArteveldehsGent</a>
        </div>
    </div>

    <?php endif; ?>
</div>

<div id="footer" class="row">
    <hr>
    <div class="large-9 columns">
    <?php print render($page['footer']); ?>
    </div>
    <div class="large-3 columns right">
        <?php if (!user_is_logged_in()): ?>
            <p><a href="<?php echo $home;?>user">Log in</a> / <a href="<?php echo $home?>user/register">Register</a></p>
            <?php else: ?>
            <p>Logged in as: <a href="<?php echo $home ?>user/<?php echo $user->uid ?>"><?php echo $user->name;?></a> / <a href="<?php echo $home?>user/logout">Log out</a></p>

        <?php endif;?>

    </div>
</div>
<script src="<?php echo $home;?><?php echo $path ?>/js/jquery.js"></script>
<script src="<?php echo $path ?>/js/foundation.js"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<?php if($url == "about"):?>
    <script src="<?php echo $path ?>/js/about.js"></script>
<?php endif;?>
<script src="<?php echo $path ?>/js/drupal.js"></script>
</body>